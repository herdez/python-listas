## Listas en Python

Las listas `lists` en Python pueden contener todo tipo de datos. Es posible trabajar con los elementos de una lista a través de la posición `índice` que ocupa determinado elemento. 


#### Índices en una lista

El índice de una lista es númerico y podemos usarlo para acceder a sus elementos.


```
list    [ 7 , 6 , 5 , 2 , 1 ]
        +---+---+---+---+---+
index     0   1   2   3   4
```

```
>>> names = ["Marcos", "Mariela", "Moni", "Julia", "Vero"]
>>> print(names[3])
Julia
```
Con el índice de la lista es posible modificar sus elementos.

```
>>> numbers = ["uno", "dos", "tres", "cuatro"]
>>> numbers[1] = 2
>>> print(numbers)
['uno', 2, 'tres', 'cuatro']
```

Para agregar elementos en una lista usamos `append()` y `extend()`.

```
>>> words = ["uno", "numero"]
>>> #solamente es posible agregar un elemento con 'append()'
... words.append("welcome")
>>> print(words)
['uno', 'numero', 'welcome']
>>> #es posible agregar más de un elemento con 'extend()'
... words.extend(["Hi", "Hello"])
>>> print(words)
['uno', 'numero', 'welcome', 'Hi', 'Hello']
```

#### Eliminación de un elemento en listas

Para eliminar elementos en las listas se usa `del`.

```
>>> numbers = [3, 5, 7, 9]
>>> del numbers[2]
>>> print(numbers)
[3, 5, 9]
```

#### Rebanado de listas en Python

Se puede obtener una porción de elementos en una lista con el uso de rangos. El resultado es una lista con los elementos que van del índice `x a y-1`.

```
"""<list>[x:y]"""

>>> numbers = [2, 4, 5, 6, 8, 9]
>>> print(numbers[1:4])
[4, 5, 6]
```

Para obtener una nueva lista con una porción de elementos siempre `x` debe ser menor que `n`:

```
>>> numbers = [2, 4, 5, 6, 8, 9]
>>> print(numbers[1:1])
[]
``` 

Cuando no se asigna el índice `x` en el rango `[x:y]` por default es `0`:

```
>>> numbers = [2, 4, 5, 6, 8, 9]
>>> print(numbers[:1])
[2]
```
Cuando no se asigna el índice `y` en el rango `[x:y]` por default es el tamaño de la lista:

```
>>> numbers = [2, 4, 5, 6, 8, 9]
>>> print(numbers[2:])
[5, 6, 8, 9]
```

Para eliminar un rango de elementos usamos `del`.

```
>>> numbers = [2, 4, 5, 6, 8, 9]
>>> del numbers[2:-1]
>>> print(numbers)
[2, 4, 9]
``` 

Para modificar un rango de elementos:

```
>>> numbers = [2, 4, 5, 6, 8, 9]
>>> numbers[-3:-1] = ["seis", "ocho"]
>>> print(numbers) 
[2, 4, 5, 'seis', 'ocho', 9] 
``` 


## Ejercicio - Agregando una lista varias veces

Define la función `several_times` que recibe como argumento dos listas y un factor entero. Este factor servirá para agregar `n` veces la segunda lista en la primera lista recibida. La función regresará una `list` como se muestra en el siguiente `driver code`: 



```python
"""several_times function"""



#driver code
print(several_times(['f', 'g', 'h'], ['i', 'j', 'k'], 2) == ['f', 'g', 'h', ['i', 'j', 'k'], ['i', 'j', 'k']])
print(several_times(['libreta', 'cuaderno'], ['hojas'], 4) == ['libreta', 'cuaderno', ['hojas'], ['hojas'], ['hojas'], ['hojas']])

```

>Tips:

- Puedes usar la siguiente referencia para saber como trabajar con listas en Python: [Understanding Lists in Python 3](https://www.digitalocean.com/community/tutorials/understanding-lists-in-python-3).
